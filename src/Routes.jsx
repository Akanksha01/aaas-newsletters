import React from 'react';
import Newsletter from './templates/Newsletter';
import ThankYou from './templates/ThankYou';
import OOPS from './templates/OOPS';
import SubscriptionFullPage from './templates/SubscriptionFullPage';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

const Routes = () => (
    <BrowserRouter>
      <Switch>
      	<Route exact path='/' component={Newsletter}/>
      	<Route exact path='/subscribe/thank-you' component={ThankYou}/>
      	<Route exact path='/subscribe/oops' component={OOPS}/>
      	<Route exact path='/subscribe/get-our-newsletters' component={SubscriptionFullPage}/>
      </Switch>
    </BrowserRouter>
)

export default Routes;
