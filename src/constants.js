export const SCIENCE_LIST = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/sci.gif",
		title : "&nbsp;<cite>Science</cite>&nbsp;Table of Contents",
		id : "Science_TOC", 
		subHeading: "Cutting-edge research in&nbsp;<em>Science</em>’s weekly issue, plus news, opinion, and more."},
	{imgURL : "//subscriptions.sciencemag.org/email/signup/sci_ec.gif",
		title : "&nbsp;<cite>Science</cite>&nbsp;Editor's Choice",
		id : "Editors_Choice",
		subHeading: "Top papers from other journals, chosen by our editors."},
	{imgURL : "//subscriptions.sciencemag.org/email/signup/sci_fr.gif",
		title : "&nbsp;First Release Notification",
		id : "Science_Express_Notification",
		subHeading: "Be the first to know about&nbsp;<em>Science</em>&nbsp;papers published online, ahead of print.&nbsp;"},
	{imgURL : "//subscriptions.sciencemag.org/email/signup/sci_twis.gif",
		title : "&nbsp;This Week in&nbsp;<cite>Science</cite>",
		id : "This_Week_In",
		subHeading: "Get a quick summary of premier papers published each week in the Science family of journals."}]

export const NEWS_FROM_SCIENCE_LIST = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/nfs_daily.gif",
		title : "&nbsp;<cite>Science</cite>&nbsp;Daily News",
		id : "Daily_News", 
		subHeading: "News from Science headlines, in your inbox daily."},
	{imgURL : "//subscriptions.sciencemag.org/email/signup/nfs_weekly.gif",
		title : "&nbsp;<cite>Science</cite>&nbsp;Weekly News Roundup",
		id : "Weekly_News_Roundup",
		subHeading: "News stories from Science’s weekly issue, including feature articles, profiles of top researchers, and news analysis."}]

export const SCIENCE_TRANSLATIONAL_MEDICINE = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/stm.gif",
		title : "&nbsp;<cite>Science Translational Medicine</cite>&nbsp;TOC",
		id : "Translational_Medicine_TOC", 
		subHeading: "The latest research in&nbsp;<em>Science Translational Medicine</em>, covering advances toward the goal of improving patients’ lives."}]

export const SCIENCE_SIGNALING = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/sig.gif",
		title : "&nbsp;<cite>Science Signaling</cite>&nbsp;TOC",
		id : "Science_Signaling_TOC", 
		subHeading: "The latest research in&nbsp;<em>Science Signaling</em>, covering cell signaling and regulation, synthetic biology, and drug discovery."}]

export const SCIENCE_ADVANCES = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/adv.gif",
		title : "&nbsp;<cite>Science Advances</cite>&nbsp;TOC",
		id : "Science_Advances_TOC", 
		subHeading: ""}]

export const SCIENCE_IMMUNOLOGY = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/imm.gif",
		title : "&nbsp;<cite>Science Immunology</cite>&nbsp;TOC",
		id : "Science_Immunology_TOC", 
		subHeading: ""}]

export const SCIENCE_ROBOTICS = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/rob.gif",
		title : "&nbsp;<cite>Science Robotics</cite>&nbsp;TOC",
		id : "Science_Robotics_TOC", 
		subHeading: ""}]

export const INTERNATIONAL = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/china.gif",
		title : "&nbsp;<cite>Science</cite>&nbsp;Roundup in Chinese",
		id : "Science_Roundup_In_Chinese", 
		subHeading: ""},
		{imgURL : "//subscriptions.sciencemag.org/email/signup/japan.gif",
		title : "&nbsp;Japan Highlights",
		id : "Japan_Highlights", 
		subHeading: ""}]

export const SCIENCE_CAREERS = [{
		imgURL : "//subscriptions.sciencemag.org/email/signup/sci_career.jpg",
		title : "&nbsp;<cite>Science</cite> Careers Job Seeker Newsletter",
		id : "Science_Careers_Job_Seeker_Newsletter", 
		subHeading: "The latest jobs from top organizations, career advice and more."}]