import renderSubscribe from './render-subscribe';

document.addEventListener('DOMContentLoaded', () => {
  renderSubscribe();
});
