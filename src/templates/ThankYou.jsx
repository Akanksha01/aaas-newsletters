import React, { Component } from 'react';

class ThankYou extends Component {

  render() {
    return (
      <div role="main" id="main-content">
        <div className="row">
          <div className="container container--content">
        	  <div className="tertiary"></div>
  					<article className="primary primary--content">
    					<div className="article__body">
  							<header className="article__header article__header--inline">
        				  <h1 className="article__headline">Thank you</h1>
                </header>
    					  <p><span>Thank you for subscribing to our newsletters, your information has been submitted successfully.</span></p>
								<p><a href="javascript:history.back()">Go back</a></p>
								<span property="dc:title" content="Thank you" className="rdf-meta element-hidden"></span>    
							</div>
  					</article>
            <div className="secondary secondary" lang="en">
        			<div id="aaas-oas_x51"></div>
  	        </div>
          </div>
    		</div>
  		</div>
    );
  }
}

export default ThankYou;
