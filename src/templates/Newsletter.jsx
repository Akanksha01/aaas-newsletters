import React, { Component } from 'react';
import Heading from '../atoms/Heading';
import RedirectLinks from '../atoms/RedirectLinks';
import NewsletterFormFields from '../molecules/NewsletterFormFields';

class Newsletter extends Component {

  render() {
    return (
      <div className="promo--newsletters">
      	<Heading className="subsection-title" title="Get Our Newsletters"/>
        <p className="priority-2">Receive emails from <cite>Science</cite>. 
        	<RedirectLinks redirectTo="/subscribe/get-our-newsletters" label="See full list" target="_blank"/>
        </p>
        <NewsletterFormFields/>
      </div>
    );
  }
}

export default Newsletter;
