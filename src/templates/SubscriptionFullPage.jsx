import React, { Component } from 'react';
import RedirectLinks from '../atoms/RedirectLinks';
import FullPageNewsletterFormFields from '../molecules/FullPageNewsletterFormFields';

class Newsletter extends Component {

  render() {
    return (
      <article className="primary--content">
    		<div className="article__body">
  				<header className="article__header article__header--inline">
          	<h1 className="article__headline">Get our newsletters</h1>
          </header>
      		<p>Enter your email address below to receive email announcements from Science. We will also send you a newsletter digest with the latest published articles.</p>
					<p>Existing subscribers manage your&nbsp;
						<RedirectLinks redirectTo="http://promo.aaas.org/ema/index.php" label="subscriptions here" target="_blank"/>.</p>
        	<FullPageNewsletterFormFields/>
          &nbsp;&nbsp;
          <p><sup>Required fields are indicated by an asterisk (*)</sup></p>
      	</div>
      </article>
    );
  }
}

export default Newsletter;
