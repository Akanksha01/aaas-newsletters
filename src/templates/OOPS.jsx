import React, { Component } from 'react';

class OOPS extends Component {

  render() {
    return (
      <div role="main" id="main-content">
        <div className="row">
          <div className="container container--content">
          	<div className="tertiary"></div>
  					<article className="primary primary--content">
    					<div className="article__body">
  							<header className="article__header article__header--inline">
          				<h1 className="article__headline">Oops</h1>
                </header>
      					<p><span>There was an issue submitting your details. Please ensure you enter a valid email address.</span></p>
								<span property="dc:title" content="Oops" className="rdf-meta element-hidden"></span>
							</div>
  					</article>
            <div className="secondary secondary" lang="en">
        			<div id="aaas-oas_x51"></div>
          	</div>
          </div>
    		</div>
  		</div>
    );
  }
}

export default OOPS;
