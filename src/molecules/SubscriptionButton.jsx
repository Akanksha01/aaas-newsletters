import React, { Component } from 'react';
import ActionButtons from '../atoms/ActionButtons';
import PropTypes from 'prop-types';

class SubscriptionButton extends Component {

  static get propTypes() {
    return {
      subscribe: PropTypes.func,
      disabled: PropTypes.bool,
      className: PropTypes.string
    };
  }

  render() {
    const {subscribe, disabled, className} = this.props;
    
    return (
        <ActionButtons 
          type="submit" 
          id="newsletter-footer-submit" 
          value="Sign up today" 
          className={className}
          onClick={subscribe}
          disabled={disabled}/>
    );
  }
}

export default SubscriptionButton;
