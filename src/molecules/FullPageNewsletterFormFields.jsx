import React, { Component } from 'react';
import axios from 'axios';
import Heading from '../atoms/Heading';
import ScienceList from '../molecules/ScienceList';
import CountryDropdown from './CountryDropdown';
import EmailField from './EmailField';
import SubscriptionButton from '../molecules/SubscriptionButton';
import * as list from '../constants';
import CheckboxInput from '../atoms/CheckboxInput';
import RedirectLinks from '../atoms/RedirectLinks';

class FullPageNewsletterFormFields extends Component {

  constructor(){
    super();
    this.state={
      checkboxChecked: {
        Science_TOC:true,
        Editors_Choice:true,
        Science_Express_Notification:true,
        This_Week_In:true,
        Daily_News: true,
        Weekly_News_Roundup: true,
        Translational_Medicine_TOC: true,
        Science_Signaling_TOC: true,
        Science_Advances_TOC: true,
        Science_Immunology_TOC: true,
        Science_Robotics_TOC: true,
        Science_Roundup_In_Chinese: false,
        Japan_Highlights: false,
        Science_Careers_Job_Seeker_Newsletter: false
      },
      consentThirdPartyEmails:false,
      consentFirstPartyEmails:false,
      email:"",
      selectedCountry: null,
      disableSubscribe: true
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.enableSubscribe = this.enableSubscribe.bind(this);
    this.subscribeRequest = this.subscribeRequest.bind(this);
    this.clearState = this.clearState.bind(this);
  }

  handleCountryChange(){
    var item = document.getElementById("newsletter-country").value;

    this.setState({
        selectedCountry: item
    });
    this.enableSubscribe();
  }

  enableSubscribe(){
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/

    if (this.state.email && this.state.selectedCountry && reg.test(this.state.email)) {
      this.setState({
        disableSubscribe: false
      });
    }else{
      this.setState({
        disableSubscribe: true
      });
    }
  }

  clearState(){
    this.setState({
      checkboxChecked: {
        Science_TOC:true,
        Editors_Choice:true,
        Science_Express_Notification:true,
        This_Week_In:true,
        Daily_News: true,
        Weekly_News_Roundup: true,
        Translational_Medicine_TOC: true,
        Science_Signaling_TOC: true,
        Science_Advances_TOC: true,
        Science_Immunology_TOC: true,
        Science_Robotics_TOC: true,
        Science_Roundup_In_Chinese: false,
        Japan_Highlights: false,
        Science_Careers_Job_Seeker_Newsletter: false
      },
      consentThirdPartyEmails:false,
      consentFirstPartyEmails:false,
      email:"",
      selectedCountry: null,
      disableSubscribe: true
    });
  }

  handleChange(e){
    if (e.target.type === "checkbox") {
      if (e.target.id === "Consent_ThirdPartyEmails") {
        this.setState({
          'consentThirdPartyEmails': e.target.checked
        })
      }else if (e.target.id === "Consent_FirstPartyEmails") {
        this.setState({
          'consentFirstPartyEmails': e.target.checked
        })
      }else{
        let checkboxChecked = Object.assign({}, this.state.checkboxChecked); 
        checkboxChecked[e.target.id] = e.target.checked;
        this.setState({checkboxChecked});
      }
    }else{
      this.setState({
        [e.target.id]: e.target.value
      });
    }
    this.enableSubscribe();
  }

  subscribe(event){
    if (this.state.email && this.state.selectedCountry.value) {
      var api_data = {'type': 'newsletter', 'emailId': this.state.email, 'Science_TOC': this.state.checkboxChecked.Science_TOC, 'Editors_Choice': this.state.checkboxChecked.Editors_Choice,'Science_Express_Notification': this.state.checkboxChecked.Science_Express_Notification, 'This_Week_In': this.state.checkboxChecked.This_Week_In,'Daily_News': this.state.checkboxChecked.Daily_News, 'Weekly_News_Roundup': this.state.checkboxChecked.Weekly_News_Roundup, 'Translational_Medicine_TOC': this.state.checkboxChecked.Translational_Medicine_TOC, 'Science_Signaling_TOC': this.state.checkboxChecked.Science_Signaling_TOC,'Science_Advances_TOC': this.state.checkboxChecked.Science_Advances_TOC, 'Science_Immunology_TOC': this.state.checkboxChecked.Science_Immunology_TOC, 'Science_Robotics_TOC': this.state.checkboxChecked.Science_Robotics_TOC, 'Science_Roundup_In_Chinese': this.state.checkboxChecked.Science_Roundup_In_Chinese,'Japan_Highlights': this.state.checkboxChecked.Japan_Highlights, 'Science_Careers_Job_Seeker_Newsletter': this.state.checkboxChecked.Science_Careers_Job_Seeker_Newsletter, 'consentThirdPartyEmails': this.state.consentThirdPartyEmails, 'consentFirstPartyEmails': this.state.consentFirstPartyEmails,'country' : this.state.selectedCountry.value, 'async_flag' : 0}
      this.subscribeRequest(api_data);
    }
    event.preventDefault();
  }

  subscribeRequest(api_data){
    var wrapper = document.getElementById("aaas-util-full-page");
    var endpoint = wrapper.getAttribute("endpoint");
    var url = endpoint;
    var subscribe_data = JSON.stringify(api_data);
    var that = this;
    axios.post(url, subscribe_data)
    .then(function(response){
      if (response.data.status==="success") {
        that.clearState();
        window.location.href="/subscribe/thank-you";
      } else{
        that.clearState();
        window.location.href="/subscribe/oops";
      }
    }).catch(error => {
        window.location.href="/subscribe/oops";
    });
  }

  render() {
    
    return (
      <form>
        <Heading title="Science"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_LIST}/>
        <Heading title="News From Science"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.NEWS_FROM_SCIENCE_LIST}/>
        <Heading title="Science Translational Medicine"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_TRANSLATIONAL_MEDICINE}/>
        <Heading title="Science Signaling"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_SIGNALING}/>
        <Heading title="Science Advances"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_ADVANCES}/>
        <Heading title="Science Immunology"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_IMMUNOLOGY}/>
        <Heading title="Science Robotics"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_ROBOTICS}/>
        <Heading title="International"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.INTERNATIONAL}/>
        <Heading title="Science Careers"/>
        <ScienceList handleChange={this.handleChange} checkboxChecked={this.state.checkboxChecked} list = {list.SCIENCE_CAREERS}/>
        <p>&nbsp;</p>

        <p>
          <label>
            <CheckboxInput id="Consent_ThirdPartyEmails" name="Consent_ThirdPartyEmails" onChangeFunc={this.handleChange}/>
            <span>I agree to receive occasional emails from AAAS/<cite>Science</cite> and <cite>Science</cite> advertisers, including information on new products, services, and special offers which may include but are not limited to product information, news, offers, career information, special reports, invitations to events and other offers.</span>
          </label>
        </p>

        <p>
          <label>
            <CheckboxInput id="Consent_FirstPartyEmails" name="Consent_FirstPartyEmails" onChangeFunc={this.handleChange}/>
            <span>I want to receive emails about AAAS/<cite>Science</cite> activities, services and publications which may include but are not limited to journal information, news, offers, career information, special reports, invitations to events, newsletters and other offers.</span>
          </label>
        </p>

        <p>
          <b>Country<sup>*</sup></b><br/>
          <CountryDropdown handleCountryChange={this.handleCountryChange} id="newsletter-country" defaultLabel="Select an option"/>
        </p>

        <p>
          <label className="element-invisible">Email</label>&nbsp;
          <EmailField value={this.state.email} handleChange={this.handleChange}/>
        </p>

        <p className="priority-2">
          Click to view the 
          <RedirectLinks redirectTo="https://www.sciencemag.org/about/privacy-policy" label=" Privacy Policy" target="_blank"/>
        </p>

        <p>
          <label className="element-invisible">Sign up today</label>
          <SubscriptionButton subscribe={this.subscribe} disabled={this.state.disableSubscribe} className="btn-cta btn-cta-large"/>
        </p>

      </form>
    );
  }
}

export default FullPageNewsletterFormFields;