import React, { Component } from 'react';
import CheckboxInput from '../atoms/CheckboxInput';
import CountryDropdown from './CountryDropdown';
import EmailField from './EmailField';
import RedirectLinks from '../atoms/RedirectLinks';
import SubscriptionButton from '../molecules/SubscriptionButton';
import axios from 'axios';

class NewsletterFormFields extends Component {

  constructor(){
    super();
    this.state={
      Science_Magazine_TOC:true,
      Science_Latest_News_and_Headlines:true,
      News_from_Science_Weekly_Headlines:true,
      Editors_Choice:true,
      Science_Express_Notification:true,
      Job_Seeker_Newsletter:true,
      consentcheckbox: false,
      email:"",
      selectedCountry: null,
      disableSubscribe: true
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.enableSubscribe = this.enableSubscribe.bind(this);
    this.subscribeRequest = this.subscribeRequest.bind(this);
    this.clearState = this.clearState.bind(this);
  }

  handleCountryChange(){
    var item = document.getElementById("newsletter-footer-country").value;
    
    this.setState({
        selectedCountry: item
    });
    this.enableSubscribe();
  }

  enableSubscribe(){
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/

    if (this.state.email && this.state.selectedCountry && reg.test(this.state.email)) {
      this.setState({
        disableSubscribe: false
      });
    }else{
      this.setState({
        disableSubscribe: true
      });
    }
  }

  clearState(){
    this.setState({
      Science_Magazine_TOC:true,
      Science_Latest_News_and_Headlines:true,
      News_from_Science_Weekly_Headlines:true,
      Editors_Choice:true,
      Science_Express_Notification:true,
      Job_Seeker_Newsletter:true,
      consentcheckbox: false,
      email:"",
      selectedCountry: null,
      disableSubscribe: true
    });
  }

  handleChange(e){
    if (e.target.type === "checkbox") {
      this.setState({
        [e.target.id]: e.target.checked,
      });
    }else{
      this.setState({
        [e.target.id]: e.target.value
      });
    }
    this.enableSubscribe();
  }

  subscribe(event){
    if (this.state.email && this.state.selectedCountry) {
      var api_data = {'type': 'newsletter', 'emailId': this.state.email, 'Science_Magazine_TOC': this.state.Science_Magazine_TOC, 'Science_Latest_News_and_Headlines': this.state.Science_Latest_News_and_Headlines,'News_from_Science_Weekly_Headlines': this.state.News_from_Science_Weekly_Headlines, 'Editors_Choice': this.state.Editors_Choice,'Science_Express_Notification': this.state.Science_Express_Notification, 'Job_Seeker_Newsletter': this.state.Job_Seeker_Newsletter, 'consentcheckbox': this.state.consentcheckbox, 'country' : this.state.selectedCountry, 'async_flag' : 0}
      this.subscribeRequest(api_data);
    }
    event.preventDefault();
  }

  subscribeRequest(api_data){
    var wrapper = document.getElementById("aaas-util-foot-2");
    var endpoint = wrapper.getAttribute("endpoint");
    var url = endpoint;
    var subscribe_data = JSON.stringify(api_data);
    var that = this;
    axios.post(url, subscribe_data)
    .then(function(response){
      if (response.data.status==="success") {
        that.clearState();
        window.location.href="/subscribe/thank-you";
      } else{
        that.clearState();
        window.location.href="/subscribe/oops";
      }
    }).catch(error => {
        window.location.href="/subscribe/oops";
    });
  }

  render() {
    
    return (
        <form>
          <div id="newsletter-footer-checkboxes">
            <p className="priority-2">
              <label>
                <CheckboxInput id="Science_Magazine_TOC" name="Science_Magazine_TOC" onChangeFunc={this.handleChange} checked={this.state.Science_Magazine_TOC}/>
                <span><cite>Science</cite> Table of Contents</span>
              </label>
            </p>
            <p className="priority-2">
              <label>
                <CheckboxInput id="Science_Latest_News_and_Headlines" name="Science_Latest_News_and_Headlines" onChangeFunc={this.handleChange} checked={this.state.Science_Latest_News_and_Headlines}/>
                <span><cite>Science</cite> Daily News</span>
              </label>
            </p>
            <p className="priority-2">
              <label>
                <CheckboxInput id="News_from_Science_Weekly_Headlines" name="News_from_Science_Weekly_Headlines" onChangeFunc={this.handleChange} checked={this.state.News_from_Science_Weekly_Headlines}/>
                <span><cite>Science</cite> News This Week</span>
              </label>
            </p>
            <p className="priority-2">
              <label>
                <CheckboxInput id="Editors_Choice" name="Editors_Choice" onChangeFunc={this.handleChange} checked={this.state.Editors_Choice}/>
                <span><cite>Science</cite> Editor's Choice</span>
              </label>
            </p>
            <p className="priority-2">
              <label>
                <CheckboxInput id="Science_Express_Notification" name="Science_Express_Notification" onChangeFunc={this.handleChange} checked={this.state.Science_Express_Notification}/>
                <span><cite>Science</cite> First Release Notification</span>
              </label>
            </p>
            <p className="priority-2">
              <label>
                <CheckboxInput id="Job_Seeker_Newsletter" name="Job_Seeker_Newsletter" onChangeFunc={this.handleChange} checked={this.state.Job_Seeker_Newsletter}/>
                <span><cite>Science</cite> Careers Job Seeker</span>
              </label>
            </p>
          </div>
          <label htmlFor="newsletter-footer-country" className="element-invisible">Country</label>
          <CountryDropdown style={{"width" : "100%", "fontSize" : ".825rem","padding" : "0.25rem" }}  handleCountryChange={this.handleCountryChange} id="newsletter-footer-country" defaultLabel="Country *"/>
          <p className="priority-2">
            <label className="element-invisible">Email</label>
            <EmailField value={this.state.email} handleChange={this.handleChange}/>
          </p>
          <p className="priority-2">
            <label>
              <CheckboxInput id="consentcheckbox" name="consentcheckbox" onChangeFunc={this.handleChange} checked={this.state.consentcheckbox}/>
              <span>I agree to receive emails from AAAS/<cite>Science</cite> and <cite>Science</cite> advertisers, including information on products, services, and special offers which may include but are not limited to news, career information, &amp; upcoming events.</span>
            </label>
          </p>
          <p className="priority-2">
            Click to view the 
            <RedirectLinks redirectTo="https://www.sciencemag.org/about/privacy-policy" label=" Privacy Policy" target="_blank"/>
          </p>
          <p className="priority-3">Required fields are indicated by an asterisk (*)</p>
          <label className="element-invisible">Sign up today</label>
          <SubscriptionButton subscribe={this.subscribe} disabled={this.state.disableSubscribe} className="btn--cta btn--cta--small"/>
        </form>
    );
  }
}

export default NewsletterFormFields;