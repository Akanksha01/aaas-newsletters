import React, { Component } from 'react';
import {countries} from '../countries.json';
import PropTypes from 'prop-types';
import Options from '../atoms/Options';

class CountryDropdown extends Component {

	static get propTypes() {
    return {
      handleCountryChange: PropTypes.func,
      id: PropTypes.string,
      defaultLabel: PropTypes.string
    };
  }

  render() {
    const { handleCountryChange, style, id, defaultLabel } = this.props;

    var countryList = countries.map((listItem, index)=>{
      return <Options key={index} value={listItem.value} label={listItem.label}/>
    })

    return (
      <select style={style} className="inpt" id={id} name="Country" onChange={() => handleCountryChange()}>
        <option defaultValue="">{defaultLabel}</option>
        {countryList}
      </select>
    );
  }
}

export default CountryDropdown;