import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScienceListings from '../atoms/ScienceListings';

class ScienceList extends Component {

	static get propTypes() {
    return {
      checkboxChecked: PropTypes.object,
      handleChange: PropTypes.func,
      list: PropTypes.array
    };
  }

  render() {
    var {handleChange, checkboxChecked, list} = this.props;
    
    var articleList = list.map((listItem, index)=>{
      return <ScienceListings val={listItem} key={index} handleChange={handleChange} checkboxChecked={checkboxChecked}/>
    })

    return (
      <ul className="item-list">
        {articleList}
      </ul>
    );
  }
}

export default ScienceList;
