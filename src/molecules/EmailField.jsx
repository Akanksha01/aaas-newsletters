import React, { Component } from 'react';
import Input from '../atoms/Input';
import PropTypes from 'prop-types';

class EmailField extends Component {

  static get propTypes() {
    return {
      value: PropTypes.string,
      handleChange: PropTypes.func
    };
  }

  render() {
  	const {value, handleChange} = this.props;

    return (
      <Input id="email" name="EmailAddress" size="35" maxlength="254" type="email" placeholder="Email address *" value={value} handleChange={handleChange}/>
    );
  }
}

export default EmailField;
