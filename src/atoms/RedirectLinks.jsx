import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RedirectLinks extends Component {

  static get propTypes() {
    return {
      label: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string
      ]).isRequired,
      redirectTo: PropTypes.string,
      className: PropTypes.string,
      target: PropTypes.string
    };
  }

  render() {
  	const {label, redirectTo, className, target} = this.props;

    return (
      <a href={redirectTo} className={className} target={target}>{label} </a>
    );
  }
}

export default RedirectLinks;
