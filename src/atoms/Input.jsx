import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Input extends Component {

  static get propTypes() {
    return {
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.string,
      placeholder: PropTypes.string,
      handleChange: PropTypes.func,
      value: PropTypes.string,
      readOnly: PropTypes.bool,
      className: PropTypes.string,
      maxLength: PropTypes.string
    };
  }

  render() {
  	const {id, name, type, placeholder, handleChange, value, readOnly, className, maxLength} = this.props;

    return (
      <input id={id} name={name} type={type} maxLength={maxLength} placeholder={placeholder} onChange={handleChange} value={value} required readOnly={readOnly} className={className}></input>
    );
  }
}

export default Input;
