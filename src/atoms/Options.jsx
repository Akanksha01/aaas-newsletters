import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Options extends Component {

  static get propTypes() {
    return {
      value: PropTypes.string,
      label: PropTypes.string
    };
  }

  render() {
    var {value, label} = this.props;

    return (
      <option value={value}>{label}</option>
    );
  }
}

export default Options;
