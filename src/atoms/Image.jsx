import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Image extends Component {

  static get propTypes() {
    return {
      alt: PropTypes.string,
      src: PropTypes.string
    };
  }

  render() {
  	const {alt, src} = this.props;

    return (
      <img alt={alt} src={src}/>
    );
  }
}

export default Image;
