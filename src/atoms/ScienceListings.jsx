import React, { Component } from 'react';
import Image from '../atoms/Image';
import PropTypes from 'prop-types';
import CheckboxInput from '../atoms/CheckboxInput';

class ScienceListings extends Component {

  static get propTypes() {
    return {
      handleChange: PropTypes.func,
      val:PropTypes.object,
      checkboxChecked: PropTypes.object      
    };
  }

  render() {
    var {val, handleChange, checkboxChecked} = this.props;
    
    var checked = checkboxChecked[val.id];

    return (
      <li>
        <article className="media media--var media--priority-2 contextual-links-region">
          <div className="media__icon">
            <Image alt="" src={val.imgURL}/>
          </div>
          <div className="media__body">
            <h2 className="media__headline">
              <label>
                <CheckboxInput id={val.id} name={val.id} className="newsletter-checkboxes" onChangeFunc={handleChange} checked={checked}/>
                <span dangerouslySetInnerHTML= {{__html: val.title}}></span>
              </label>
            </h2>
            <p className="media__deck" dangerouslySetInnerHTML= {{__html: val.subHeading}}></p>
          </div>
        </article>
      </li>
    );
  }
}

export default ScienceListings;
