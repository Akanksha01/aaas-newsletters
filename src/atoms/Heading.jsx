import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Heading extends Component {

  static get propTypes() {
    return {
      className: PropTypes.string,
      title: PropTypes.string
    };
  }

  render() {
  	const {className, title} = this.props;

    return (
      <h2 className={className}>{title}</h2>
    );
  }
}

export default Heading;
