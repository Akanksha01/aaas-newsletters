import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ActionButtons extends Component {

  static get propTypes() {
    return {
      type: PropTypes.string,
      id: PropTypes.string,
      value: PropTypes.string,
      className: PropTypes.string,
      disabled: PropTypes.bool,
      onClick: PropTypes.func
    };
  }

  render() {
    const {id, type, value, className, onClick, disabled} = this.props;

    return (
	    <button type={type} id={id} value={value} className={className} onClick={onClick} disabled={disabled}>{value}</button>
    );
  }
}

export default ActionButtons;
