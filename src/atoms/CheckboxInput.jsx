import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CheckboxInput extends Component {

  static get propTypes() {
    return {
      id: PropTypes.string,
      name: PropTypes.string,
      onChangeFunc: PropTypes.func,
      checked: PropTypes.bool,
      className: PropTypes.string
    };
  }

  render() {
  	const {id, name, onChangeFunc, checked, className} = this.props;

    return (
      <input id={id} name={name} type="checkbox" onChange={onChangeFunc} checked={checked} className={className}/>
    );
  }
}

export default CheckboxInput;
