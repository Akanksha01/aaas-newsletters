import React from 'react';
import ReactDom from 'react-dom';
import Newsletter from './templates/Newsletter';
import SubscriptionFullPage from './templates/SubscriptionFullPage';

function renderSubscribe() {
	if (document.getElementById("aaas-util-foot-2")) {
		var wrapper = document.getElementById("aaas-util-foot-2");
		ReactDom.render(<Newsletter/>, wrapper);
	};
	
	if (document.getElementById("aaas-util-full-page")) {
		var wrapper = document.getElementById("aaas-util-full-page");
		ReactDom.render(<SubscriptionFullPage/>, wrapper);
	};
}
	
export default renderSubscribe;