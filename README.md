# Science Search Newsletter Subscription Module

This repo contains the react components for AAAS Newsletter-Subscription.

## Prerequites/Dependencies

- Node 8 (dev) - [Can be managed via nvm](https://github.com/creationix/nvm)
- [webpack](https://github.com/webpack) installed globally.
- [webpack-cli](https://github.com/webpack/webpack-cli) installed globally.

## Install

- If you don't have webpack installed globally yet, `npm install webpack -g`
- If you don't have webpack-cli installed globally yet, `npm install webpack-cli -g`
- Install libraries `npm install`

## Compile

For production, you should compile down a static version of the site using the following command.

`npm run build`

This will create a compiled and minified version of the Subscription react components in this repository (`src/lib/subscribe.js`).

## Subscription Module Integration

The html components for integrating react login module should be written in the following format:
```
<script type="text/javascript" src="subscribe.js"></script>

<div class="secondary" id="aaas-util-foot-2"></div>

```

##### Description
- The div where the component is used should be declared with the same format as given above. The id and class attribute can vary according to the type declared.
