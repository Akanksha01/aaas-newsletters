const webpack = require('webpack');
const path = require('path');

const webpackConfig = {
  entry: {
    'subscribe': './src/aaas-subscribe.jsx'
  },

  output: {
    path: path.resolve(__dirname, './src/lib'),
    filename: '[name].js'
  },

  resolve: {
    extensions: ['.js', '.jsx']
  },

  mode: 'production',

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            'react',
            ['env', {
              targets: {
                browsers: [
                  'last 2 versions',
                  'IE >= 11',
                ],
              },
            }],
          ],
        },
      }
    ],
  },

  plugins: [
    // Moment.js bundles every possible locale, this selects only english
    // https://github.com/moment/moment/issues/2517#issuecomment-185836313
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/)
  ],
};

  if (process.env.NODE_ENV === 'production') {
    webpackConfig.plugins.push(new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }));
  }

module.exports = webpackConfig;
